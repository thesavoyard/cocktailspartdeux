package com.example.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.domain.model.Category
import com.example.domain.model.DrinkDetails

@Database(entities = [Category::class, DrinkDetails::class], version = 1)
abstract class DrinksDB: RoomDatabase() {

    abstract fun drinkDao(): DrinkDao

    companion object {
        private const val DATABASE_NAME = "drinks.db"

        @Volatile
        private var instance: DrinksDB? = null

        fun getInstance(context: Context): DrinksDB {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also {
                    instance = it
                }
            }
        }

        private fun buildDatabase(context: Context): DrinksDB {
            return Room
                .databaseBuilder(context, DrinksDB::class.java, DATABASE_NAME)
                .build()
        }
    }
}