package com.example.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.cocktailsthesequal.data.remote.dto.drink_details.Drink
import com.example.domain.model.Category
import com.example.domain.model.DrinkDetails
import kotlinx.coroutines.flow.Flow


@Dao
interface DrinkDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCategories(categories: Array<Category>)

    @Query("SELECT * FROM category")
    fun getCategories(): Flow<List<Category>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDrinkDetails(drink: DrinkDetails?)

    @Query("SELECT * FROM drink_details WHERE idDrink = :idDrink")
    fun getDrinkDetails(idDrink: String): DrinkDetails?
}