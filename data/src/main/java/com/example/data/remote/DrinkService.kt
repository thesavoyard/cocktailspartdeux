package com.example.data.remote


import com.example.cocktailsthesequal.data.remote.dto.categories.CategoriesDTO
import com.example.cocktailsthesequal.data.remote.dto.drink_details.DrinkDetails
import com.example.cocktailsthesequal.data.remote.dto.drinks_by_category.DrinksByCategoryDTO
import com.example.common.Constants.CATEGORIES_ENDPOINT
import com.example.common.Constants.DRINKS_IN_CATEGORY
import com.example.common.Constants.ONE_DRINK_BY_ID
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface DrinkService {

    @GET(CATEGORIES_ENDPOINT)
    suspend fun getCategories(@Query("c") c: String = "list"): Response<CategoriesDTO>

    @GET(DRINKS_IN_CATEGORY)
    suspend fun getDrinksByCategory(@Query("c") name: String): Response<DrinksByCategoryDTO>

    @GET(ONE_DRINK_BY_ID)
    suspend fun getOneDrinkById(@Query("i") drinkId: String): DrinkDetails
}