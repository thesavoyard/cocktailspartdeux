package com.example.data.di

import com.example.data.local.DrinksDB
import com.example.data.remote.RemoteDataSources
import com.example.data.repository.DrinkRepositoryImpl
import com.example.domain.repository.DrinkRepository
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val dataModule = module {
    single { DrinksDB.getInstance(androidContext()).drinkDao() }

    single { RemoteDataSources.getCocktailService() }

    single<DrinkRepository> { DrinkRepositoryImpl(get(), get()) }
}