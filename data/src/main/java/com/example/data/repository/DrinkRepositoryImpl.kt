package com.example.data.repository


import com.example.cocktailsthesequal.data.remote.dto.drink_details.Drink
import com.example.domain.model.DrinkDetails
import com.example.cocktailsthesequal.data.remote.dto.drinks_by_category.DrinksByCategoryDTO
import com.example.cocktailsthesequal.data.remote.dto.drinks_by_category.toDrinksByCategory
import com.example.data.local.DrinkDao
import com.example.data.remote.DrinkService
import com.example.domain.model.Category
import com.example.domain.model.DrinksByCategory
import com.example.domain.repository.DrinkRepository
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import retrofit2.Response

class DrinkRepositoryImpl constructor(
    private val drinkService: DrinkService,
    private val drinkDao: DrinkDao,
    private val scope: CoroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.IO)
) : DrinkRepository {

    @OptIn(ExperimentalCoroutinesApi::class)
    override suspend fun getCategories(): Flow<List<Category>>? =
        callbackFlow {
            fetchCategories().collectLatest { dbCategories ->
                val categoryResponse = dbCategories.getOrNull()
                if (categoryResponse?.isEmpty() == true) {
                    val saveCategory = async { categoryMap() }
                    if (saveCategory != null) {
                        scope.launch {
                            saveCategory.await()
                                ?.let { drinkDao.insertCategories(*it.toTypedArray()) }
                            saveCategory.await()?.let { send(it) }
                        }
                    }
                } else {
                    if (categoryResponse != null) {
                        send(categoryResponse)
                    }
                }
            }
        }

    override suspend fun getDrinkById(idDrink: String): DrinkDetails? =
        withContext(Dispatchers.IO) {
            val cachedDrinkDetails = drinkDao.getDrinkDetails(idDrink)
            if (cachedDrinkDetails?.strDrink != null) {
                return@withContext cachedDrinkDetails
            } else {
                val drink = remoteDrinkDetails(idDrink)
                drinkDao.insertDrinkDetails(drink)
                return@withContext drink
            }

        }


    override suspend fun getDrinksByCategory(categoryName: String): DrinksByCategory? =
        withContext(Dispatchers.IO) {
            val response: Response<DrinksByCategoryDTO> =
                drinkService.getDrinksByCategory(categoryName)
            if (response.isSuccessful && response.body() is DrinksByCategoryDTO) {
                val result = response.body()!!.toDrinksByCategory()
                return@withContext result
            } else {
                return@withContext null
            }
        }


    private suspend fun fetchCategories(): Flow<Result<List<Category>>> = callbackFlow {
        drinkDao.getCategories()
            .catch() { error ->
                send(Result.failure(error))
            }.collectLatest { categories ->
                send(Result.success(categories))
            }
    }

    private suspend fun categoryMap(): List<Category>? {
        val remoteCategories = drinkService.getCategories().body()?.drinks?.map {
            Category(
                strCategory = it.strCategory
            )
        }

        return remoteCategories
    }

    private suspend fun remoteDrinkDetails(idDrink: String): DrinkDetails? {
        val response = drinkService.getOneDrinkById((idDrink))
        val drink = response.drinks.map {
            DrinkDetails(
                idDrink = response.drinks[0].idDrink,
                strAlcoholic = response.drinks[0].strAlcoholic,
                strCategory = response.drinks[0].strCategory,
                strCreativeCommonsConfirmed = response.drinks[0].strCreativeCommonsConfirmed,
                strDrink = response.drinks[0].strDrink,
                strDrinkAlternate = response.drinks[0].strDrinkAlternate,
                strDrinkThumb = response.drinks[0].strDrinkThumb,
                strGlass = response.drinks[0].strGlass,
                strImageAttribution = response.drinks[0].strImageAttribution,
                strIBA = response.drinks[0].strIBA,
                strImageSource = response.drinks[0].strImageSource,
                strIngredient1 = response.drinks[0].strIngredient1,
                strIngredient2 = response.drinks[0].strIngredient2,
                strIngredient3 = response.drinks[0].strIngredient3,
                strIngredient4 = response.drinks[0].strIngredient4,
                strIngredient5 = response.drinks[0].strIngredient5,
                strIngredient6 = response.drinks[0].strIngredient6,
                strIngredient7 = response.drinks[0].strIngredient7,
                strIngredient8 = response.drinks[0].strIngredient8,
                strIngredient9 = response.drinks[0].strIngredient9,
                strIngredient10 = response.drinks[0].strIngredient10,
                strIngredient11 = response.drinks[0].strIngredient11,
                strIngredient12 = response.drinks[0].strIngredient12,
                strIngredient13 = response.drinks[0].strIngredient13,
                strIngredient14 = response.drinks[0].strIngredient14,
                strIngredient15 = response.drinks[0].strIngredient15,
                strInstructions = response.drinks[0].strInstructions,
                strMeasure1 = response.drinks[0].strMeasure1,
                strMeasure2 = response.drinks[0].strMeasure2,
                strMeasure3 = response.drinks[0].strMeasure3,
                strMeasure4 = response.drinks[0].strMeasure4,
                strMeasure5 = response.drinks[0].strMeasure5,
                strMeasure6 = response.drinks[0].strMeasure6,
                strMeasure7 = response.drinks[0].strMeasure7,
                strMeasure8 = response.drinks[0].strMeasure8,
                strMeasure9 = response.drinks[0].strMeasure9,
                strMeasure10 = response.drinks[0].strMeasure10,
                strMeasure11 = response.drinks[0].strMeasure11,
                strMeasure12 = response.drinks[0].strMeasure12,
                strMeasure13 = response.drinks[0].strMeasure13,
                strMeasure14 = response.drinks[0].strMeasure14,
                strMeasure15 = response.drinks[0].strMeasure15,
                strTags = response.drinks[0].strTags,
                strVideo = response.drinks[0].strVideo

            )
        }

        return drink[0]

    }

}