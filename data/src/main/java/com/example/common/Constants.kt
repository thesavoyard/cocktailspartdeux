package com.example.common

object Constants {

    const val BASE_URL = "https://www.thecocktaildb.com"
    const val CATEGORIES_ENDPOINT = "/api/json/v1/1/list.php"
    const val DRINKS_IN_CATEGORY = "/api/json/v1/1/filter.php"
    const val ONE_DRINK_BY_ID = "/api/json/v1/1/lookup.php"

}