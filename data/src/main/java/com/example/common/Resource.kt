package com.example.common

sealed class Resource<T>(val data: T? = null, val message: String? = null) {
    class Success<T> (data: T): Resource<T>(data)
    class Error<T>(message: String) : Resource<T>(data = null, message)
    class Loading<T>(data: T? = null) : Resource<T>(data)
    class Idle<T>(data: T? = null) : Resource<T>(data)
}

//sealed class JimmiesResource<T>(val data: T) {
//    class Success<T> (data: T): Resource<T>(data)
//    class Error<Nothing>(message: String) : Resource<Nothing>()
//    object Loading : Resource<Nothing>()
//    object Idle: Resource<Nothing>()
//}
