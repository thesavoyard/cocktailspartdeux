package com.example.domain.model

import com.example.cocktailsthesequal.data.remote.dto.drink_details.Drink


data class Categories(
    val categories: List<Drink> = listOf()

)
