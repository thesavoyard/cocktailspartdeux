package com.example.cocktailsthesequal.data.remote.dto.drinks_by_category

import com.example.cocktailsthesequal.data.remote.dto.drink_details.Drink
import com.example.domain.model.DrinksByCategory

data class DrinksByCategoryDTO(
    val drinks: List<Drink>
)

fun DrinksByCategoryDTO.toDrinksByCategory(): DrinksByCategory {
    return DrinksByCategory(
        drinks = drinks
    )
}