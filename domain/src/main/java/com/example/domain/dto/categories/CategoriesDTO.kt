package com.example.cocktailsthesequal.data.remote.dto.categories

import com.example.cocktailsthesequal.data.remote.dto.drink_details.Drink
import com.example.domain.model.Categories


data class CategoriesDTO(
    val drinks: List<Drink>
)

fun CategoriesDTO.toCategories(): Categories {
    return Categories(
        categories = drinks
    )
}