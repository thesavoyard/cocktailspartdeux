package com.example.domain.repository

import com.example.cocktailsthesequal.data.remote.dto.categories.CategoriesDTO
import com.example.cocktailsthesequal.data.remote.dto.drink_details.Drink
import com.example.domain.model.Categories
import com.example.domain.model.Category
import com.example.domain.model.DrinkDetails
import com.example.domain.model.DrinksByCategory
import kotlinx.coroutines.flow.Flow
import retrofit2.Response

interface DrinkRepository {

    suspend fun getCategories(): Flow<List<Category>>?

    suspend fun getDrinksByCategory(categoryName: String): DrinksByCategory?

    suspend fun getDrinkById(idDrink: String): DrinkDetails?
}