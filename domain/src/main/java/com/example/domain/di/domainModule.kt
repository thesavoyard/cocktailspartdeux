package com.example.domain.di

import GetDrinksByCategoryUseCase
import com.example.domain.use_case.get_categories.GetCategoriesUseCase
import com.example.domain.use_case.get_drink_details.GetDrinkDetailsUseCase
import org.koin.dsl.module

val domainModule = module {
    single {GetDrinksByCategoryUseCase(get())}
    single {GetCategoriesUseCase(get())}
    single { GetDrinkDetailsUseCase(get()) }
}