package com.example.domain.use_case.get_drink_details

import android.util.Log
import com.example.cocktailsthesequal.data.remote.dto.drink_details.Drink
import com.example.domain.model.DrinkDetails
import com.example.domain.repository.DrinkRepository

class GetDrinkDetailsUseCase (
    private val repository: DrinkRepository,
) {

    //    private val repository: DrinkRepository = DrinkRepositoryImpl(api)

//    suspend fun execute(drinkId: String): Flow<Resource<DrinkDetails>> = flow {
//        try {
//            emit(Resource.Loading())
//            val getDrinkById = repository.getDrinkById(drinkId)
//            emit(Resource.Success(getDrinkById))
//        } catch (e: HttpException) {
//            emit(Resource.Error(e.localizedMessage ?: "An unexpected error occurred."))
//        } catch (e: IOException) {
//            emit(Resource.Error("Could not reach server, check your internet connection."))
//        }
//    }

    suspend operator fun invoke(drinkId: String): DrinkDetails?{
        val response = repository.getDrinkById(drinkId)
//        Log.e("USE CASE", "invoke: $response ", )
        return response
    }
}