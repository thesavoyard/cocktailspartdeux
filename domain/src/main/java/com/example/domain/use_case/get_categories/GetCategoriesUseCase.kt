package com.example.domain.use_case.get_categories

import com.example.cocktailsthesequal.data.remote.dto.categories.CategoriesDTO
import com.example.domain.model.Categories
import com.example.domain.model.Category
import com.example.domain.repository.DrinkRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import retrofit2.Response

class GetCategoriesUseCase(
    private val repository: DrinkRepository,
    private val scope: CoroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.IO)
) {


    suspend operator fun invoke(): Flow<Result<List<Category>>>? = callbackFlow {
        val job = scope.launch {
            repository.getCategories()?.catch { collector ->
                send(Result.failure(collector))
            }?.collectLatest { categories ->
                send(Result.success(categories))
            }
        }
        awaitClose { job.cancel() }
    }
}