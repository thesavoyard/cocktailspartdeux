

import com.example.domain.model.DrinksByCategory
import com.example.domain.repository.DrinkRepository

class GetDrinksByCategoryUseCase(
    private val repository: DrinkRepository,
) {

    //    private val repository: DrinkRepository = DrinkRepositoryImpl(api)

//    suspend operator fun invoke(categoryName: String): Flow<Resource<DrinksByCategory>> = flow {
//        try {
//            emit(Resource.Loading())
//            val drinksByCategory = repository.getDrinksByCategory(categoryName).toDrinksByCategory()
//            emit(Resource.Success(drinksByCategory))
//        } catch (e: HttpException) {
//            emit(Resource.Error(e.localizedMessage ?: "An unexpected error occurred."))
//        } catch (e: IOException) {
//            emit(Resource.Error("Could not reach server, check your internet connection."))
//        }
//    }
    suspend operator fun invoke(categoryName: String): DrinksByCategory? {
        return repository.getDrinksByCategory(categoryName)
    }
}