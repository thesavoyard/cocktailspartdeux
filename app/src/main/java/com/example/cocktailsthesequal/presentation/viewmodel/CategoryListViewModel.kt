package com.example.cocktailsthesequal.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.common.Resource
import com.example.cocktailsthesequal.data.remote.dto.categories.CategoriesDTO
import com.example.cocktailsthesequal.presentation.view.fragments.categories.CategoryListState
import com.example.domain.model.Categories
import com.example.domain.model.Category
import com.example.domain.use_case.get_categories.GetCategoriesUseCase
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch


class CategoryListViewModel(
    private val getCategoriesUseCase: GetCategoriesUseCase
) : ViewModel() {

    private val _categoriesListState: MutableStateFlow<CategoryListState> = MutableStateFlow(
        CategoryListState()
    )
    val categoriesListState get() = _categoriesListState.asStateFlow()


    fun getCategories() {
        viewModelScope.launch {
            getCategoriesUseCase()?.catch { collector ->

            }?.collectLatest { category ->
                    val categories = category.getOrNull()
                if (category != null) {
                    _categoriesListState.value = CategoryListState(
                        categories = categories
                    )
                }
            }


//            if (result.isSuccessful) {
//                _categoriesListState.value = Resource.Success(result.body()!!)
//            } else {
//                _categoriesListState.value = Resource.Error("An Error Occured")
//            }

//            when(result) {
//                is Response -> {
//                    _categoriesListState.value = CategoryListState(categories = result.data as Categories?)
//                }
//                is Resource.Error<*> -> {
//                    _categoriesListState.value = CategoryListState(error = result.message ?: "An unexpected error occurred.")
//                }
//                is Resource.Loading<*> -> {
//                    _categoriesListState.value = CategoryListState(isLoading = true)
//                }
//                else -> "Hey!"
//            }
        }
    }
}