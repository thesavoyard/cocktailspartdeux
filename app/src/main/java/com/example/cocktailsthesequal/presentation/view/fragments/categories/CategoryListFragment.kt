package com.example.cocktailsthesequal.presentation.view.fragments.categories

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cocktailsthesequal.databinding.CategoryListBinding
import com.example.cocktailsthesequal.presentation.utils.lifeCycleFlow
import com.example.cocktailsthesequal.presentation.view.adapters.CategoryListAdapter
import com.example.cocktailsthesequal.presentation.viewmodel.CategoryListViewModel
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel


class CategoryListFragment : Fragment() {

    private var _binding: CategoryListBinding? = null
    private val binding get() = _binding!!
    private val viewModel by viewModel<CategoryListViewModel>()
    lateinit var categoryListState: CategoryListState

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = CategoryListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch {
            viewModel.getCategories()
            initViews()
        }
    }

    private fun initViews() = with(binding) {
        lifeCycleFlow(viewModel.categoriesListState) { state ->
            when (state) {
                is CategoryListState -> {
                    val cat = state.categories
                    cat?.let { categories ->
                        val categoriesListAdapter =
                            CategoryListAdapter(state.categories) { categoryName: String ->
                                val action =
                                    CategoryListFragmentDirections.actionCategoryListFragmentToDrinksByCategoryFragment(
                                        categoryName
                                    )
                                findNavController().navigate(action)
                            }
                        rvCategories.adapter = categoriesListAdapter
                    }
                }
                else -> {
                    Log.d("error", "initViews: Something went wrong")
                }
            }
            rvCategories.layoutManager = LinearLayoutManager(context)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}