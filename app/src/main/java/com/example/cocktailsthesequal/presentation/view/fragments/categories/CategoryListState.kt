package com.example.cocktailsthesequal.presentation.view.fragments.categories

import com.example.domain.model.Categories
import com.example.domain.model.Category
import kotlinx.coroutines.flow.Flow

data class CategoryListState(
    val categories: List<Category>? = listOf(),
)
