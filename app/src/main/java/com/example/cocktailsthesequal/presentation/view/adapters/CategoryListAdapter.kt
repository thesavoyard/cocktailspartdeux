package com.example.cocktailsthesequal.presentation.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cocktailsthesequal.data.remote.dto.drink_details.Drink
import com.example.cocktailsthesequal.databinding.CategoryItemBinding
import com.example.cocktailsthesequal.presentation.view.fragments.categories.CategoryListState
import com.example.domain.model.Categories
import com.example.domain.model.Category

class CategoryListAdapter(
    val categories: List<Category>,
    val onItemClicked: (String) -> Unit
): RecyclerView.Adapter<CategoryListAdapter.CategoriesViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriesViewHolder {
        return CategoriesViewHolder(
            CategoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: CategoriesViewHolder, position: Int) {
        val category = categories[position]
        holder.displayCategories(category)
        holder.onItemClicked = onItemClicked
    }

    override fun getItemCount(): Int = categories.size

    class CategoriesViewHolder(var binding: CategoryItemBinding) :
    RecyclerView.ViewHolder(binding.root){
        var onItemClicked: ((String) -> Unit)? = null


        fun displayCategories(category: Category) {
            binding.tvCategoryItem.text = category.strCategory

            binding.tvCategoryItem.setOnClickListener{
                onItemClicked?.invoke(category.strCategory)
            }
        }
    }

}