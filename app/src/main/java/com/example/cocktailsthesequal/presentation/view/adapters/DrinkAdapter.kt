package com.example.cocktailsthesequal.presentation.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cocktailsthesequal.databinding.IngredientsItemBinding

class DrinkAdapter(
    val ingredients: List<String> = listOf()
): RecyclerView.Adapter<DrinkAdapter.IngredientsViewHolder>() {

    class IngredientsViewHolder(val binding: IngredientsItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun displayIngredients(ingredients: String) {
            binding.tvIngredients.text = ingredients
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IngredientsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = IngredientsItemBinding.inflate(layoutInflater, parent, false)
        return IngredientsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: IngredientsViewHolder, position: Int) {
        val ingredient = ingredients.get(position)
        holder.displayIngredients(ingredient)
    }

    override fun getItemCount(): Int = ingredients.size
}