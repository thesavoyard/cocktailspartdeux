package com.example.cocktailsthesequal.presentation.viewmodel

import GetDrinksByCategoryUseCase
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.domain.use_case.get_categories.GetCategoriesUseCase
import com.example.domain.use_case.get_drink_details.GetDrinkDetailsUseCase

class GenericVMFactory(
    private val getDrinksByCategoryUseCase: GetDrinksByCategoryUseCase,
    private val getCategoriesUseCase: GetCategoriesUseCase,
    private val getDrinkDetailsUseCase: GetDrinkDetailsUseCase
) : ViewModelProvider.NewInstanceFactory() {
    //Constructor needs the other usecases.


    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        return when (modelClass.simpleName) {
            DrinksByCategoryViewModel::class.java.simpleName -> {
                DrinksByCategoryViewModel(getDrinksByCategoryUseCase) as T
            }
            DrinkDetailsViewModel::class.java.simpleName -> {
                DrinkDetailsViewModel(getDrinkDetailsUseCase) as T
            }
            else -> {
                CategoryListViewModel(getCategoriesUseCase) as T
            }
        }
    }
}