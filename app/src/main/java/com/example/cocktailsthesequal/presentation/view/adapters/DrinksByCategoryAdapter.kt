package com.example.cocktailsthesequal.presentation.view.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.*
import com.bumptech.glide.Glide
import com.example.cocktailsthesequal.data.remote.dto.drink_details.Drink
import com.example.cocktailsthesequal.databinding.ByCategoryItemBinding

class DrinksByCategoryAdapter(
    private val context: Context,
    var drinksByCategory: List<Drink>,
    private val onDrinkClicked: (String) -> Unit = {}
) : Adapter<DrinksByCategoryAdapter.DrinksViewHolder>() {

    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinksViewHolder {
        return DrinksViewHolder(
            ByCategoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: DrinksViewHolder, position: Int) {
        val drink = drinksByCategory[position]
//        Log.e("In Adapter", "onBindViewHolder: $drink", )
        holder.displayDrinks(drink)
        holder.imageDisplay(drink.strDrinkThumb, context)
        holder.onDrinkClicked = onDrinkClicked

    }

    override fun getItemCount(): Int = drinksByCategory.size

    class DrinksViewHolder(var binding: ByCategoryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var onDrinkClicked: ((String) -> Unit)? = null
        fun imageDisplay(image: String, context: Context) {
            Glide.with(context)
                .load(image)
                .into(binding.ivCategoryCocktail)
        }

        
        fun displayDrinks(drink: Drink) {
            val TAG = "Drinks Adapter"

            binding.tvCategoryCocktail.text = drink.strDrink

            binding.ivCategoryCocktail.setOnClickListener {

                val clicked = drink.idDrink
                onDrinkClicked?.invoke(clicked)

            }

            binding.tvCategoryCocktail.setOnClickListener {

                val clicked = drink.idDrink
                onDrinkClicked?.invoke(clicked)

            }
        }
    }

}