package com.example.cocktailsthesequal.presentation.view.fragments.drinkdetails

import com.example.cocktailsthesequal.data.remote.dto.drink_details.DrinkDetails
import com.example.domain.model.DrinksByCategory


data class DrinkDetailsState(
    val isLoading: Boolean = false,
    val drinkDetails: DrinkDetails? = null,
    val error: String = ""
)
