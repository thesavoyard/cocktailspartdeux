package com.example.cocktailsthesequal.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktailsthesequal.data.remote.dto.drink_details.Drink
import com.example.domain.model.DrinkDetails
import com.example.domain.use_case.get_drink_details.GetDrinkDetailsUseCase
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch


class DrinkDetailsViewModel(
    private val getDrinkDetailsUseCase: GetDrinkDetailsUseCase
) : ViewModel() {

    private val _drinkDetailsState: MutableStateFlow<DrinkDetails?> = MutableStateFlow(
        DrinkDetails()
    )
    val drinkDetailsState get() = _drinkDetailsState.asStateFlow()

    fun getDrinkDetails(drinkId: String) {
        viewModelScope.launch {
            val result = getDrinkDetailsUseCase(drinkId)
            _drinkDetailsState.value = result
//            if (result.isSuccessful) {
//                _drinkDetailsState.value = Resource.Success(result.body()!!)
//            } else {
//                _drinkDetailsState.value = Resource.Error("An error occurred.")
//            }
//            when (result) {
//                is Resource.Success<*> -> {
//                    _drinkDetailsState.value =
//                        DrinkDetailsState(drinkDetails = result.data as DrinkDetails?)
//                }
//                is Resource.Error<*> -> {
//                    _drinkDetailsState.value = DrinkDetailsState(
//                        error = result.message ?: "An unexpected error occurred."
//                    )
//                }
//                is Resource.Loading<*> -> {
//                    _drinkDetailsState.value = DrinkDetailsState(isLoading = true)
//                }
//            }
        }
    }
}