package com.example.cocktailsthesequal.presentation.viewmodel

import GetDrinksByCategoryUseCase
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.model.DrinksByCategory
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch


class DrinksByCategoryViewModel (
    private val getDrinksByCategoryUseCase: GetDrinksByCategoryUseCase
) : ViewModel() {
    val TAG = "BY CATEGORY VIEW MODEL"
    private val _drinksByCategoryState: MutableStateFlow<DrinksByCategory?> = MutableStateFlow(
        DrinksByCategory()
    )

    val drinksByCategoryState get() = _drinksByCategoryState.asStateFlow()



    fun getDrinksByCategory(categoryName: String) {
        viewModelScope.launch {
            val result = getDrinksByCategoryUseCase(categoryName)
//            Log.e(TAG, "getDrinksByCategory: $result", )
            _drinksByCategoryState.value = result

//            if (result.isSuccessful) {
//                _drinksByCategoryState.value = Resource.Success(result.body()!!)
//            } else {
//                _drinksByCategoryState.value = Resource.Error("An error occurred")
//            }

//            when (result) {
//                is Resource.Success<*> -> {
//                    _drinksByCategoryState.value =
//                        DrinksByCategoryState(drinksByCategory = result.data as DrinksByCategory?)
//                }
//                is Resource.Error<*> -> {
//                    _drinksByCategoryState.value = DrinksByCategoryState(
//                        error = result.message ?: "An unexpected error occurred."
//                    )
//                }
//                is Resource.Loading<*> -> {
//                    _drinksByCategoryState.value = DrinksByCategoryState(isLoading = true)
//                }
//            }
        }
    }
}