package com.example.cocktailsthesequal.presentation.utils

import GetDrinksByCategoryUseCase
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.example.cocktailsthesequal.presentation.viewmodel.GenericVMFactory
import com.example.data.remote.RemoteDataSources
import com.example.data.repository.DrinkRepositoryImpl
import com.example.domain.use_case.get_categories.GetCategoriesUseCase
import com.example.domain.use_case.get_drink_details.GetDrinkDetailsUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest

fun <T> Fragment.lifeCycleFlow(flow: Flow<T>, collect: suspend (T) -> Unit) {
    lifecycleScope.launchWhenStarted {
        flow.collectLatest(collect)
    }
}

//val vmFactory = GenericVMFactory(
//    getDrinksByCategoryUseCase = GetDrinksByCategoryUseCase(repository = DrinkRepositoryImpl(drinkService = RemoteDataSources.getCocktailService())),
//    getDrinkDetailsUseCase = GetDrinkDetailsUseCase(repository = DrinkRepositoryImpl(drinkService = RemoteDataSources.getCocktailService())),
//    getCategoriesUseCase = GetCategoriesUseCase(repository = DrinkRepositoryImpl(drinkService = RemoteDataSources.getCocktailService()))
//)