package com.example.cocktailsthesequal.presentation.view.fragments.drinksbycategory

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cocktailsthesequal.databinding.ByCategoryBinding
import com.example.cocktailsthesequal.presentation.utils.lifeCycleFlow
import com.example.cocktailsthesequal.presentation.view.adapters.DrinksByCategoryAdapter
import com.example.cocktailsthesequal.presentation.viewmodel.DrinksByCategoryViewModel
import com.example.domain.model.DrinksByCategory
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class DrinksByCategoryFragment : Fragment() {
    private var _binding: ByCategoryBinding? = null
    private val binding get() = _binding!!
    private val viewModel by viewModel<DrinksByCategoryViewModel>()
    lateinit var drinksByCategoryState: DrinksByCategoryState
    val args by navArgs<DrinksByCategoryFragmentArgs>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ByCategoryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch {
            viewModel.getDrinksByCategory(args.category)
            initViews()
        }
    }

    fun initViews() = with(binding) {
        rvInCategory.layoutManager = LinearLayoutManager(context)

        lifeCycleFlow(viewModel.drinksByCategoryState) { state ->
            when (state) {
                is DrinksByCategory -> {
                    val byCat = state.drinks
//                    Log.e("Fragment", "initViews: $byCat")
                    byCat.let { drinksByCategory ->
                        val drinksByCategoryAdapter =
                            DrinksByCategoryAdapter(requireContext(), drinksByCategory) { drinkId ->
                                val action =
                                    DrinksByCategoryFragmentDirections.actionDrinksByCategoryFragmentToDrinkDetailsFragment(
                                        drinkId
                                    )
                                findNavController().navigate(action)
                            }
                        rvInCategory.adapter = drinksByCategoryAdapter
                    }
                }
                else -> {
                    Log.d("error", "initViews: Something went wrong")
                }
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}