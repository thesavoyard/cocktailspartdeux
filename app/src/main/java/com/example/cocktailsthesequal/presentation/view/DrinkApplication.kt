package com.example.cocktailsthesequal.presentation.view

import com.example.cocktailsthesequal.di.appModule
import com.example.data.di.dataModule
import com.example.domain.di.domainModule
import com.google.android.play.core.splitcompat.SplitCompatApplication
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin


class DrinkApplication : SplitCompatApplication() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@DrinkApplication)
            modules(
                dataModule,
                domainModule,
                appModule

            )
        }
    }

}