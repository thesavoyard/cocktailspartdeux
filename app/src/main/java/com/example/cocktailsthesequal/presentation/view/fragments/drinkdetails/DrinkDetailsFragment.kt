package com.example.cocktailsthesequal.presentation.view.fragments.drinkdetails

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.cocktailsthesequal.data.remote.dto.drink_details.Drink
import com.example.cocktailsthesequal.databinding.DrinkDetailBinding
import com.example.cocktailsthesequal.presentation.utils.lifeCycleFlow
import com.example.cocktailsthesequal.presentation.view.adapters.DrinkAdapter
import com.example.cocktailsthesequal.presentation.viewmodel.DrinkDetailsViewModel
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class DrinkDetailsFragment : Fragment() {
    private var _binding: DrinkDetailBinding? = null
    private val binding get() = _binding!!
    val args by navArgs<DrinkDetailsFragmentArgs>()
    lateinit var drinkDetailsState: DrinkDetailsState
    private val viewModel by viewModel<DrinkDetailsViewModel>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DrinkDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch {
            viewModel.getDrinkDetails(args.drinkId)
            initViews()
        }
    }

    fun initViews() = with(binding) {
        lifeCycleFlow(viewModel.drinkDetailsState) { state ->
            val ingredients = state?.toIngredientsList()
            Log.e("DRINKDETAILS INGREDIENTS", "initViews: $ingredients", )
            tvCocktail.text = state?.strDrink
            context?.let {
                Glide.with(it)
                    .load(state?.strDrinkThumb)
                    .into(imgCocktail)
            }
            tvCocktailDescription.text = state?.strInstructions
            rvIngredients.layoutManager = LinearLayoutManager(context)
            rvIngredients.adapter = ingredients?.let { DrinkAdapter(it as List<String>) }
        }
    }
}