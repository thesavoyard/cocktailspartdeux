package com.example.cocktailsthesequal.presentation.view.fragments.drinksbycategory

import com.example.domain.model.DrinksByCategory

data class DrinksByCategoryState(
    val isLoading: Boolean = false,
    val drinksByCategory: DrinksByCategory? = null,
    val error: String = ""
)
