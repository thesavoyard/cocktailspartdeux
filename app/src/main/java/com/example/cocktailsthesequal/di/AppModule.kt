package com.example.cocktailsthesequal.di

import com.example.cocktailsthesequal.presentation.viewmodel.CategoryListViewModel
import com.example.cocktailsthesequal.presentation.viewmodel.DrinkDetailsViewModel
import com.example.cocktailsthesequal.presentation.viewmodel.DrinksByCategoryViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    viewModel{CategoryListViewModel(get())}
    viewModel{DrinkDetailsViewModel(get())}
    viewModel{DrinksByCategoryViewModel(get())}
}